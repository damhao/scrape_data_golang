package interfaces

import "scrape/models"

type PostService interface {
	GetPosts(url string, params ...int) []models.Post
	GetPost() *models.Post
	SavePost() bool
}
