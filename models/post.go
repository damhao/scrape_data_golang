package models

type Post struct {
	ID          int          `json:"id"`
	URL         string       `json:"url"`
	Title       string       `json:"title,omitempty"`
	Description string       `json:"description,omitempty"`
	Thumbnail   string       `json:"thumbnail,omitempty"`
	Detail      []PostDetail `json:"detail,omitempty"`
	Topic       string
}

type PostTable struct {
	ID          int `gorm:"primaryKey"`
	URL         string
	Title       string `sql:"type:CHARACTER SET utf8 COLLATE utf8_general_ci"`
	Description string `sql:"type:CHARACTER SET utf8 COLLATE utf8_general_ci"`
	Thumbnail   string
	Detail      string `sql:"type:CHARACTER SET utf8 COLLATE utf8_general_ci"`
	Topic       string `sql:"type:CHARACTER SET utf8 COLLATE utf8_general_ci"`
}

func (PostTable) TableName() string {
	return "data_vn"
}

type PostDetail struct {
	Type        string `json:"type"`
	Value       string `json:"value"`
	Description string `json:"description,omitempty"`
}

type DBConfig struct {
	ServerName string
	User       string
	Password   string
	DB         string
}
