package configs

import (
	"fmt"

	"github.com/spf13/viper"
)

var (
	TOPICS = []string{"kinh-doanh", "the-gioi", "phap-luat", "the-thao"}
)

func GetConfig() *viper.Viper {
	//fmt.Println("=====")

	config := viper.New()
	config.SetConfigName("config")
	config.AddConfigPath("./configs")
	err := config.ReadInConfig()
	if err != nil {
		fmt.Println("Config not found", err)
		return nil
	}
	return config

}
