package services

import (
	"io"
	"net/http"
	"regexp"
	"scrape/models"
	"scrape/utils"
	"strconv"

	"github.com/PuerkitoBio/goquery"
	"github.com/anaskhan96/soup"
	"gorm.io/gorm"
)

type PostService struct {
	Post     *models.Post
	Database *gorm.DB
}

func (m *PostService) GetPosts(url string, params ...int) []models.Post {
	var ps []models.Post

	targetPage := 1
	currentPage := 1
	if len(params) > 0 {
		targetPage = params[0]
	}
	if len(params) > 1 {
		currentPage = params[1]
	}

	newUrl := "https://vnexpress.net/" + url
	if targetPage > 0 {
		newUrl += "-p" + strconv.Itoa(currentPage)
	}

	req, err := http.Get(newUrl)
	if err != nil {
		return nil
	}

	req.Header.Set("User-Agent", "Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/104.0.0.0 Safari/537.36\n")

	body, err := io.ReadAll(req.Body)
	if err != nil {
		return nil
	}

	doc := soup.HTMLParse(string(body))
	titles := doc.FindAll("", "class", "title-news")
	for _, title := range titles {
		postURL := title.Find("a").Attrs()["href"]

		var re = regexp.MustCompile(`(-)([0-9]+){1}(.html)`)
		matches := re.FindStringSubmatch(postURL)
		postID, _ := strconv.Atoi(matches[2])

		ps = append(ps, models.Post{
			ID:    postID,
			URL:   postURL,
			Topic: url,
		})
	}

	if len(ps) > 0 && targetPage > currentPage {
		return append(ps, m.GetPosts(url, targetPage, currentPage+1)...)
	}

	return ps
}

func (ps *PostService) GetPost() *models.Post {

	req, err := http.Get(ps.Post.URL)
	if err != nil {
		return nil
	}
	req.Header.Set("User-Agent", "Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/104.0.0.0 Safari/537.36")

	doc, err := goquery.NewDocumentFromReader(req.Body)
	if err != nil {
		return nil
	}

	ps.Post.Title = doc.Find("h1.title-detail").Text()

	doc.Find("p.description span.location-stamp").Remove()
	ps.Post.Description = doc.Find("p.description").Text()

	ps.Post.Thumbnail, _ = doc.Find("meta[itemprop=\"thumbnailUrl\"]").Attr("content")

	doc.Find("article.fck_detail").Children().Each(func(index int, selection *goquery.Selection) {
		className, classExist := selection.Attr("class")
		if !classExist {
			return
		}
		detail := models.PostDetail{}

		switch className {
		case "Normal":
			detail.Type = "text"
			detail.Value = selection.Text()
		case "tplCaption":
			detail.Type = "image"
			image := selection.Find("img[itemprop=\"contentUrl\"]")
			detail.Value, _ = image.Attr("data-src")
			detail.Description, _ = image.Attr("alt")
		}

		if detail.Type == "" {
			return
		}
		ps.Post.Detail = append(ps.Post.Detail, detail)
	})
	return ps.Post
}

func (ps *PostService) SavePost() bool {
	detail, _ := utils.JSON_Marshal(ps.Post.Detail)

	if string(detail) == "null" {
		detail = nil
	}

	site := models.PostTable{
		ID:          ps.Post.ID,
		URL:         ps.Post.URL,
		Title:       ps.Post.Title,
		Description: ps.Post.Description,
		Thumbnail:   ps.Post.Thumbnail,
		Detail:      string(detail),
		Topic:       ps.Post.Topic,
	}

	var count int64
	result := ps.Database.Model(&models.PostTable{}).Where("id = ?", ps.Post.ID).Count(&count)

	if count >= 1 {
		err := ps.Database.Model(&site).Updates(&site).Error
		if err != nil {
			return false
		}
	} else {
		err := ps.Database.Create(&site).Error
		if err != nil {
			return false
		}
	}

	if result.RowsAffected > 0 {
		return true
	}
	return false
}
