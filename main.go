package main

import (
	"fmt"
	"scrape/configs"
	services "scrape/controller"
	"scrape/database"
	"scrape/models"
	"time"
)

func main() {

	//start := time.Now()

	database.Init()
	var db = database.Connector

	fmt.Println(db)

	postChannel := make(chan models.Post)

	var ps services.PostService

	// WORKER POOL
	for i := 0; i < 10; i++ {
		go func(i int) {

			for post := range postChannel {

				fmt.Printf("Worker %d dang xu li post nay \n", i)

				var ps = services.PostService{
					Post:     &post,
					Database: db,
				}

				ps.GetPost()
				fmt.Println("ID: ", post.ID)
				fmt.Println("Title: ", post.Title)
				fmt.Println("Description: ", post.Description)
				fmt.Println("==========================================================")
				//}

			}
		}(i)
	}

	//for {
	for _, topic := range configs.TOPICS {
		posts := ps.GetPosts(topic, 1)

		for _, post := range posts {
			post.Topic = topic
			postChannel <- post
		}
	}

	time.Sleep(10 * time.Second)
	//}

	//fmt.Println("Total time :", time.Since(start))
}
