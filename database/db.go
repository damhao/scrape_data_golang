package database

import (
	"fmt"
	"log"
	cfg "scrape/configs"

	"scrape/models"

	"gorm.io/driver/mysql"
	"gorm.io/gorm"
)

// Connector variable
var Connector *gorm.DB

func Init() {

	c := cfg.GetConfig()
	servername := c.GetString("ServerName")
	user := c.GetString("User")
	password := c.GetString("Password")
	db := c.GetString("DB")

	config :=
		models.DBConfig{
			ServerName: servername,
			User:       user,
			Password:   password,
			DB:         db,
		}

	connectionString := GetConnectionString(config)
	err := Connect(connectionString)
	if err != nil {
		panic(err.Error())
	}
	Connector.AutoMigrate(
		//&models.Post{},
		&models.PostTable{},
		//&models.PostDetail{},
	)
	log.Println("Tables migrated")

}

func Connect(connectionString string) error {
	//fmt.Println("=====")
	var err error
	Connector, err = gorm.Open(mysql.Open(connectionString), &gorm.Config{
		// SkipDefaultTransaction: true,
		PrepareStmt: true,
	})
	if err != nil {
		return err
	}

	log.Println("Connection was successful!!")
	return nil

}

var GetConnectionString = func(config models.DBConfig) string {
	connectionString := fmt.Sprintf("%s:%s@tcp(%s)/%s?charset=utf8&parseTime=True&loc=Local",
		config.User, config.Password, config.ServerName, config.DB)
	return connectionString
}
